/**
 * @file
 * On the configuration page.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.ebourgognetf = {
    attach: function (context, settings) {
      /*
      * Display the link for each eforms in the list.
      */

      // If the user select a teleform in the list.
      $('#edit-formlist').change(
                function () {
                  // Retrieve url for the selected form.
                  var linkUrl = $(this).val();

                  // Display the textfield to enter the link label.
                  $('#linkUrl').html(linkUrl);
                }
            );
    }
  };
})(jQuery);
